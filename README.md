# CDJ-800 MKII USB MIDI converted

This project aims at converting a Pioneer CDJ 800 MKII into a fully compatible MIDI controller. This project involves modifying the enclosure of the Pioneer CDJ 800 MKII and also soldering on the original internal PCBs. This project is inspired by previous work done by [DJ LEGION](https://youtu.be/LSgzjqSXjl8). Feel free to fork this project and modify it to suit your MIDI conversion project. 

## MCU

Teensy 3.6 - MK66FX1M0VMD18

## Repository description 

### What is <span style="color:green">working?</span>

* Play, Cue
* Fx selection and activation
* Browing and loading
* Jogwheels, Scratching, Bending
* Cycle Temporange
* Loops
* Beat loops
* LED feeback

### What is <span style="color:red">not working </span>?

* Screen handling (not connected)

### Build instructions

1. Install <b>Teensyduino IDE</b> or the <b>Arduino IDE with the Teensy add on</b> by following this guide : [TDownload and Install Teensy support into the Arduino IDE (pjrc.com)](https://www.pjrc.com/teensy/td_download.html)
2. Plug your Teensy board via a USB cable (micro USB)
3. Open <b>Teensyduino IDE</b> or the <b>Arduino IDE (with the Teensy add on installed</b>) 
4. Open the <b>.ino</b> file of your interest in this project
5. Select `Teensy 3.6` under `Tools>Board`
6. Select `MIDI`under `Tools>USB Type`
7. Select the current USB port used by your Teensy board under `Tools>Port`
8. Click on the `Verify` icon on the IDE 
9. If you have some warnings during verification, check your board configuration done in steps 5,6 and 7
10. Upload the program to the Teensy board by clicking on the `Upload` button

### Testing instructions

1. Install the open source software <b>Mixxx</b>
2. Locate the folder in wich <b>Mixxx</b> stores all MIDI mappings
> In MAC Os it is generally under : `/Users/<your_username>/Library/Application Support/Mixxx/controllers`

3. Copy and paste the `.xml` and the `.js` file  in it

4. Plug the CDJ 800MKII via a USB cable
5. Open <b>Mixxx</b> and go to :
    `preferences->Controllers->Teensy MIDI`
    You can then load the preset made for the Teensy MIDI by clicking on `Load Preset` drop down menu.
6. Click apply and relaunch <b> Mixxx</b>
7. You are ready to <b> Mixxx</b> ! 

### Modifying instructions

1. Using <b>Teensyduino IDE</b>, open the file `src/cdj800mk2_config.h`
2. Adapt the code to your own pinout configuration 
3. Try to compile (while crossing your fingers) and if it goes wrong check your code and modify the src.ino if needed

### Resources

MCU information: 
- [Teensy official documentation](https://www.pjrc.com/store/teensy36.html)

Related projects:
- [CDJ 1000MKIII MIDI Conversion using Teensy 3.6](https://github.com/midicdj1000/teensy3.6-mk3)
- [CDJ 800MKII MIDI Conversion using Teensy 3.2](https://imgur.com/a/o7jd8) 

# Library used

- <b>Encoder: </b> Used to handle jog movement reading (for Scratch and Bending) 
- <b>Bounce2: </b> Used to handle the 'debouncing' of the buttons so they all deliver a clean boolean signal
- <b>usbMIDI: </b> Used to handle MIDI communication by exposing functions to send, receive and handle MIDI notes, MIDI conctrole changes...

## Authors

- Laurie MALARET
- Suzanne DUCROT
- Axel DEFEZ
- Aviran TETIA
