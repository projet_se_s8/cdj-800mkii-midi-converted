#define ENCODER_OPTIMIZE_INTERRUPTS
#include "Bounce2.h"
#include <Encoder.h>
#include "cdj800mk2_config.h"

/********************************************************
 *  =============== MIDI CONVERTED CDJ ===================
 *  This program sends MIDI note when a button of the
 *  microcontroller is pushed. MIDI messages can be both
 *  sent (through MIDI channel 1) and received 
 *  (tthrough MIDI channel 2). 
 *  
 *  MAKE SURE TO CREATE CONFIG FILE AND INCLUDE IT
 *  USE "cjd800mk2_config" as an example
 *  
 *  Based on teensy example codes for MIDI communication:
 *  https://www.pjrc.com/teensy/td_midi.html
 *  ======================================================
 **********************************************************/
//#define DEBUG

#define START_NOTE          60 // C3 as starting note for buttons
#define DEBOUNCE_INTERVAL   5
#define JOG_FWD_MIDI      65
#define JOG_BWD_MIDI      63
#define JOG_FWD_MIDI_FST  66
#define JOG_BWD_MIDI_FST  62
#define JOG_PRESSED_MIDI  34
#define JOG_RELEASED_MIDI 33

// MIDI channel declaration
const int outChannel = 1;
const int inChannel = 1; 

// Debug LED
int ledPin = LED_BUILTIN;

// Declare a "debounced" version of every buttons
Bounce debouncedBtn[BUTTON_NUMBER] ;

// Instanciate encoder object
Encoder myEncoders(encodList[0], encodList[1]);

// Timing var for encoder reading
unsigned long newtime;
unsigned long oldtime = 0;
long wheelpos  = -999;
int wheelmove = 0;
long vel = 0;

// Analog read variables
uint8_t oldAnalog[ANA_INPUTS_NUMBER];
uint8_t newAnalog[ANA_INPUTS_NUMBER];

// the setup routine runs once when you press reset:
void setup() {
  
  for (int i=0; i<ANA_INPUTS_NUMBER;i++){
    oldAnalog[i]=0;
    newAnalog[i]=0;
  }

  #ifdef DEBUG
  // open the serial port at 9600 bps:
  Serial.begin(9600);
  Serial.println("Scratch Test:");
  #endif

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); 
  
  // Init all buttons as debounced buttons (to get "clean" falling edges signal from each button press)
  for(int i=0; i < BUTTON_NUMBER; i++){ 
    debouncedBtn[i].attach(btnList[i], INPUT_PULLUP);
    debouncedBtn[i].interval(DEBOUNCE_INTERVAL); // 5 = 5 ms debounce time
  }


  // Init all LEDs as outputs and flash them
  for(int i=0; i < LED_NUMBER; i++){ 
    pinMode(ledList[i].pin, OUTPUT);
  }
  blinkCycleLED(true);
  delay(500);
  blinkCycleLED(false);
  
  // Handler attachement
  usbMIDI.setHandleNoteOff(OnNoteOff);
  usbMIDI.setHandleNoteOn(OnNoteOn) ;

  digitalWrite(LED_BUILTIN, LOW);
}

// the loop routine runs over and over again forever:
void loop(){
  // Read incomming MIDI messages
  usbMIDI.read();

  // Read/Write MIDI button states
  rwBtn();

  // Read/Write analogpins state
  rwAnalog();
  
  // Read/Write Jog Encoder
  rwJog();
  
  // Send the MIDI packet
  usbMIDI.send_now();

  delay(1);
}

void OnNoteOn(byte channel, byte note, byte velocity)
{
  if(channel == inChannel){
    for(int i=0; i<LED_NUMBER; i++){
      if(note == ledList[i].note){
        digitalWrite(ledList[i].pin, HIGH); // Note-On turns on LED
      }
    }
  }
}

void OnNoteOff(byte channel, byte note, byte velocity)
{
  if(channel == inChannel){
    for(int i=0; i<LED_NUMBER; i++){
      if(note == ledList[i].note){
        digitalWrite(ledList[i].pin, LOW); // Note-Off turns Off LED
      }
    }
  }
}
void rwBtn(void){
  // Send a noteOn MIDI message when button pressed
  for(int i=0; i < BUTTON_NUMBER; i++){ 
    debouncedBtn[i].update();
    if (debouncedBtn[i].fell()) { // When button is pushed
      usbMIDI.sendNoteOn(START_NOTE+i, 127, outChannel);  // 60 = C4
      digitalWrite(LED_BUILTIN, HIGH); // Note-On turns on LED
    #ifdef DEBUG  
      blinkCycleLED(true);
    #endif
      
    } 
    // Send a noteOff MIDI message when button released
    if (debouncedBtn[i].rose()) {
      //usbMIDI.sendNoteOff(START_NOTE+i, 0, outChannel);  // 60 = C4
      usbMIDI.sendNoteOn(START_NOTE+i, 0, outChannel);  // 60 = C4
      digitalWrite(LED_BUILTIN, LOW); // Note-On turns on LED
     #ifdef DEBUG   
      blinkCycleLED(false);
     #endif
    }
  }
  // Send the MIDI packet
  usbMIDI.send_now();
}

void rwJog(void){
  long newwheel;
  newtime = millis();
  newwheel = myEncoders.read();
  wheelmove = 0;
  vel = 0;
  int jogPressed = digitalRead(e_JogPress);

  if (newwheel < wheelpos) {                                   // If the wheel went forward
    //vel = ((newwheel - wheelpos)  / (newtime - oldtime)) *2 ;
    //if (vel>60) vel =60;
    //if (vel <=0 ) vel = 0;
    if (jogPressed==0){
      usbMIDI.sendControlChange(JOG_PRESSED_MIDI, JOG_FWD_MIDI, outChannel);
     #ifdef DEBUG
      Serial.print("PRESSED : Encoder speed:");
      Serial.println(vel); 
      digitalWrite(LED_BUILTIN, HIGH); // Note-On turns on LED
     #endif
    
    } else {
      usbMIDI.sendControlChange(JOG_RELEASED_MIDI, JOG_FWD_MIDI, outChannel);
     #ifdef DEBUG
      Serial.print("NOT PRESSED : Encoder speed:");
      Serial.println(127); 
      digitalWrite(LED_BUILTIN, LOW); // Note-On turns on LED
     #endif
    
    }
    wheelpos = newwheel;
  }
  if (newwheel > wheelpos) {                                  // If the wheel went backward
    //vel = ((wheelpos - newwheel) / (newtime - oldtime)) * 2;
      // if (vel>60) vel = 60;
       //if (vel <=0 ) vel = 0;
      if (jogPressed==0){
        usbMIDI.sendControlChange(JOG_PRESSED_MIDI, JOG_BWD_MIDI, outChannel);
     #ifdef DEBUG
      digitalWrite(LED_BUILTIN, HIGH); // Note-On turns on LED
     #endif
      } else {
        usbMIDI.sendControlChange(JOG_RELEASED_MIDI, JOG_BWD_MIDI, outChannel);
      #ifdef DEBUG
        digitalWrite(LED_BUILTIN, LOW); // Note-On turns on LED
      #endif
      }
    wheelpos = newwheel;
    
  }
  oldtime = newtime;
}

void blinkCycleLED(boolean state){
  if(state){
    for(int i=0; i < LED_NUMBER; i++){
      digitalWrite(ledList[i].pin, HIGH); // Note-On turns on LED 
    }
  }else
  {
    for(int i=0; i < LED_NUMBER; i++){
      digitalWrite(ledList[i].pin, LOW); // Note-On turns on LED 
    } 
  }
}

void rwAnalog(void){
  // Read Analog from pitch strip
  for(int i=0; i<ANA_INPUTS_NUMBER;i++){
    newAnalog[i]=(analogRead(anaInList[i])/8); //analogread: [0;1023] to changecontrol value :[0;127]
  
    if(newAnalog[i] != oldAnalog[i]){
      if(abs(newAnalog[i]-oldAnalog[i]) > PITCH_MVMNT_THRESHOLD ){
        usbMIDI.sendControlChange(MIN_GENERAL_CONTROLLER+i, newAnalog[i], outChannel);
        //usbMIDI.sendPitchBench(newAnalog, outChannel);
        oldAnalog[i]=newAnalog[i];
      }
    }
  }
}
