#ifndef CDJ800MK2_CONFIG_H
#define CDJ800MK2_CONFIG_H

/*************************************************************
 *  =================== CONFIG FILE =========================
 *  This file is the config file which needs to be adapted 
 *  to your pin configuration and your type of CDJ components 
 *  ======================================================
 *************************************************************/
// Define here the number of buttons, leds and analogue inputs
#define   BUTTON_NUMBER     28
#define   LED_NUMBER        14
#define   ANA_INPUTS_NUMBER 2

// In case of oscillation when pitchbend is moving
#define PITCH_MVMNT_THRESHOLD 0

//Minimal ControlChange value for Analog values 
#define MIN_GENERAL_CONTROLLER 16


// Jogmovement threshold 
#define MVMNTTHRESH         100000

typedef struct {
  int pin;
  int note;
}t_led;

// Define here the list of your buttons and write the pin number dedicated next to it
enum BUTTON_LIST {
  e_Play        = 0,
  e_Cue         = 1,
  e_RSearch     = 2,
  e_FSearch     = 3,
  e_FTSearch    = 4,
  e_RTSearch    = 5, 
  e_Rev         = 6,
  e_Loop8       = 7,
  e_Loop4       = 8,
  e_Loop2       = 9,
  e_Loop1       = 10,
  e_LoopIn      = 11,
  e_LoopOut     = 12,
  e_Time        = 14,
  e_Text        = 15,
  e_LFolder     = 16,
  e_RFolder     = 17,
  e_QuickReturn = 18,
  e_LCall       = 19,
  e_RCall       = 20,
  e_Delete      = 23,
  e_Memory      = 24,
  e_Eject       = 25,
  e_JogMode     = 26,
  e_Tempo       = 27,
  e_MastTempo   = 28,
  e_JogPress    = 36,
  e_Reloop      = 37
};

//========================
//      Note list
//========================
enum NOTE_LIST{
  e_note_Play        = 60,
  e_note_Cue         ,
  e_note_RSearch     ,
  e_note_FSearch     ,
  e_note_FTSearch    ,
  e_note_RTSearch    , 
  e_note_Rev         ,
  e_note_Loop8       ,
  e_note_Loop4       ,
  e_note_Loop2       ,
  e_note_Loop1       ,
  e_note_LoopIn      ,
  e_note_LoopOut     ,
  e_note_Time        ,
  e_note_Text        ,
  e_note_LFolder     ,
  e_note_RFolder     ,
  e_note_QuickReturn ,
  e_note_LCall       ,
  e_note_RCall       ,
  e_note_Delete      ,
  e_note_Memory      ,
  e_note_Eject       ,
  e_note_JogMode     ,
  e_note_Tempo       ,
  e_note_MastTempo   ,
  e_note_JogPress    ,
  e_note_Reloop      
};

const int btnList[]={
  e_Play        ,
  e_Cue         ,
  e_RSearch     ,
  e_FSearch     ,
  e_FTSearch    ,
  e_RTSearch    ,
  e_Rev         ,
  e_Loop8       ,
  e_Loop4       ,
  e_Loop2       ,
  e_Loop1       ,
  e_LoopIn      ,
  e_LoopOut     ,
  e_Time        ,
  e_Text        ,
  e_LFolder     ,
  e_RFolder     ,
  e_QuickReturn ,
  e_LCall       ,
  e_RCall       ,
  e_Delete      ,
  e_Memory      ,
  e_Eject       ,
  e_JogMode     ,
  e_Tempo       ,
  e_MastTempo   ,
  e_JogPress    ,
  e_Reloop
};

// Define here the list of your analog inputs and write the pin number dedicated next to it
enum ANALOG_LIST {
  e_SpeedAdj    = 31,
  e_PitchTempo  = 32
};

enum ENCODER_LIST{
  e_Encoder2    = 38,
  e_Encoder1    = 39,
  e_noLed       = 255, 
};

const int encodList[]{
  e_Encoder2,
  e_Encoder1 
};

const int anaInList[]={
  e_SpeedAdj    ,
  e_PitchTempo  
};

// Define here the list of your LEDs and write the pin number dedicated next to it
enum LED_LIST {
  e_PlayLED     = 29,
  e_CueLED      = 30,
  e_ReloopLED   = 33,
  e_LoopOutLED  = 34,
  e_LoopInLED   = 35,
  e_RevLED      = 41,
  e_Loop8LED    = 44,
  e_Loop4LED    = 46,
  e_Loop1LED    = 48,
  e_QuickRetLED = 50,
  e_JogModeLED  = 51,
  e_TempoLED    = 53,
  e_MastTempLED = 54,
  e_Loop2LED    = 56  
};

const t_led ledList[]={
  {e_PlayLED, e_note_Play} ,
  {e_CueLED, e_note_Cue}      ,
  {e_ReloopLED, e_note_Reloop}  ,
  {e_LoopOutLED, e_note_LoopOut} ,
  {e_LoopInLED, e_note_LoopIn}   ,
  {e_RevLED , e_note_Rev}     ,
  {e_Loop8LED, e_note_Loop8}    ,
  {e_Loop4LED, e_note_Loop4}    ,
  {e_Loop2LED , e_note_Loop2} ,
  {e_Loop1LED, e_note_Loop1}     ,
  {e_QuickRetLED ,e_note_QuickReturn},
  {e_JogModeLED  ,e_note_JogMode},
  {e_TempoLED    ,e_note_Tempo},
  {e_MastTempLED,  e_note_MastTempo}    
};



#endif
