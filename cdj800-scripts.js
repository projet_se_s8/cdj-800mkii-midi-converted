var cdj800 = {};

cdj800.vinylMode = true;
cdj800.tempoRanges = [0.06, 0.10, 0.16, 0.25];

var vinylModeCount = 0;
const NOTEON = 0x90;
const LEDON = 0x7F;
const LEDOFF = 0x00;
const JOGMODENOTE = 0x53;

cdj800.init = function (id, debugging) {
    // Init vinl mode LED
    if(cdj800.vinylMode) midi.sendShortMsg(NOTEON,JOGMODENOTE,LEDON);
    // Init tempo range at 10%
    engine.setValue('[Channel1]','rateRange',cdj800.tempoRanges[1]);
}

cdj800.shutdown = function() {
   // turn off all LEDs
   for (var i = 60; i <= 87; i++) {
        midi.sendShortMsg(0x90, i, LEDOFF);
    }
}

cdj800.switchTempoRange = function(channel, control, value, status, group){
    var currRange = engine.getValue(group, 'rateRange');
    var idx = 0;
    if(value == 0) return; // Ignore button release
    for(var i = 0; i < cdj800.tempoRanges.length; i++){
        if(currRange == cdj800.tempoRanges[i]){
            idx = (i + 1) % cdj800.tempoRanges.length;
            break;
        }
    }
    engine.setValue('[Channel1]', 'rateRange', cdj800.tempoRanges[idx]);
    if(idx==1){
      midi.sendShortMsg(NOTEON, control, LEDON);
    }else{
      midi.sendShortMsg(NOTEON, control, LEDOFF);
    }
}

// The button that enables/disables scratching
cdj800.wheelTouch = function (channel, control, value, status, group) {
    var deckNumber = script.deckFromGroup(group);
    if (value != 0 && cdj800.vinylMode) {  // Some wheels send 0x90 on press and release, so you need to check the value
        var alpha = 1.0/8;
        var beta = alpha/32;
        engine.scratchEnable(deckNumber, 1048, 33+1/3, alpha, beta);
    } else {    // If button up
      
        engine.scratchDisable(deckNumber);
    }
}

// The wheel that actually controls the scratching
cdj800.wheelTurn = function (channel, control, value, status, group) {
    // B: For a control that centers on 0x40 (64):
    var newValue = value - 64;
    print("====turns====");  
    // In either case, register the movement
    var deckNumber = script.deckFromGroup(group);
    if (engine.isScratching(deckNumber)) {
        engine.scratchTick(deckNumber, newValue); // Scratch!
    } else {
        print("=======Bending!====");
        engine.setValue(group, 'jog', newValue); // Pitch bend
    }
}

cdj800.setVinylMode = function(channel, control, value, status, group){
  if(value == 0x7F){
    cdj800.vinylMode = !cdj800.vinylMode;
    print("Vinyl mode : ");
    print(cdj800.vinylMode);
    if(cdj800.vinylMode) midi.sendShortMsg(NOTEON,control,LEDON);
    else midi.sendShortMsg(NOTEON,control, LEDOFF);
  }
}
