
/********************************************************
 *  =============== MIDI DEMONSTRATOR ===================
 *  This program sends MIDI note when a button is pushed
 *  on pin (31) and light up an LED hooked on pin (32)
 *  when a D3 MIDI note of type "noteOn" is recieved on 
 *  outChannel 
 *  
 *  Based on teensy example codes for MIDI communication:
 *  https://www.pjrc.com/teensy/td_midi.html
 *  ======================================================
 **********************************************************/
 
#include "Bounce2.h"

#define DEBOUNCE_INTERVAL 25
#define MIDI_NOTE_SENT    60 // C3
#define MIDI_NOTE_RECV    62 // D3

// MIDI channel declaration
const int outChannel = 1;
const int inChannel = 2; 


// Pin declaration 
const int btnPin = 24;
const int ledPin = 13;
const int analogPin = A17;

// Declare a "debounced" version of our btn
Bounce myBtn = Bounce(); // 5 = 5 ms debounce time

//Autres variables utiles
int oldpitch=0;
int newpitch=0;

 
void OnNoteOn(byte channel, byte note, byte velocity)
{
  if(channel == inChannel){
    if(note == MIDI_NOTE_RECV){ // If the note is a D
      digitalWrite(ledPin, HIGH); // Note-On turns on LED
    }
  }
}

void OnNoteOff(byte channel, byte note, byte velocity)
{
  if(channel == inChannel){
    if(note == MIDI_NOTE_RECV){ // If the note is a D
      digitalWrite(ledPin, LOW); // Note-Off turns off LED
    }
  }
}

// the setup routine runs once when you press reset:
void setup() {
  // Pin configuration
  pinMode(ledPin, OUTPUT); 
  myBtn.attach(btnPin,INPUT_PULLUP);
  myBtn.interval(DEBOUNCE_INTERVAL); // Use a debounce interval of 25 milliseconds
 
  // Handler attachement
  usbMIDI.setHandleNoteOff(OnNoteOff);
  usbMIDI.setHandleNoteOn(OnNoteOn) ; 
}

// the loop routine runs over and over again forever:
void loop() {
  // Read incomming MIDI messages
  usbMIDI.read();
  
  // Send a noteOn MIDI message when button pressed
  myBtn.update();
  if (myBtn.fell()) { // When button is pushed
    usbMIDI.sendNoteOn(MIDI_NOTE_SENT, 99, outChannel);
  } 
  // Send a noteOff MIDI message when button released
  if (myBtn.rose()) {
    usbMIDI.sendNoteOff(MIDI_NOTE_SENT, 0, outChannel);  
  }
  newpitch=(analogRead(analogPin)<<4)-8192; //analogread: [0;1023] to pitchBend :[-8 192;8 191]
  if(newpitch != oldpitch){
    usbMIDI.sendPitchBend(newpitch, outChannel);
    oldpitch=newpitch;
  }
  
  // Send the MIDI packet
  usbMIDI.send_now();

  delay(1);
}
